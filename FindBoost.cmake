# see https://cmake.org/cmake/help/v3.11/module/FindBoost.html

set(COMPONENT_PROPERTIES 
    INTERFACE_LINK_LIBRARIES
    INTERFACE_INCLUDE_DIRECTORIES
    INTERFACE_COMPILE_DEFINITIONS
    INTERFACE_COMPILE_OPTIONS
)

message(STATUS "Custom FindBoost.cmake for monolithic conan boost")

function(component_to_library component)
    set(all_libs ${CONAN_LIBS_BOOST})
    unset(LIB_${component} CACHE)
    list(FILTER all_libs INCLUDE REGEX "[a-zA-Z0-9]+_${component}.*")
    foreach(lib ${all_libs})
        unset(found_lib CACHE)
        find_library(found_lib ${lib} PATHS ${CONAN_LIB_DIRS_BOOST} NO_DEFAULT_PATH)

        if(found_lib)
            if(NOT DEFINED LIB_${component})
                set(LIB_${component} ${found_lib} PARENT_SCOPE)
            else()
                list(APPEND LIB_${component} ${found_lib})
            endif()
        endif()
    endforeach()
endfunction(component_to_library)

set(Boost_ALL_COMPONENTS
accumulators
algorithm
align
any
array
asio
assert
assign
atomic
beast
bimap
bind
call_traits
callable_traits
chrono
circular_buffer
compatibility
compressed_pair
compute
concept_check
config
container
container_hash
context
contract
conversion
convert
core
coroutine
coroutine2
crc
date_time
dll
dynamic_bitset
enable_if
endian
exception
fiber
filesystem
flyweight
foreach
format
function
function_types
functional
fusion
geometry
gil
graph
graph_parallel
hana
heap
histogram
hof
icl
identity_type
in_place_factory
integer
interprocess
interval
intrusive
io
iostreams
iterator
lambda
lexical_cast
local_function
locale
lockfree
log
math
member_function
metaparse
min_max
move
mp11
mpi
mpl
msm
multi_array
multi_index
multiprecision
numeric_conversion
numeric_interval
numeric_odeint
numeric_ublas
operators
optional
ostream_string
outcome
parameter
parameter_python
phoenix
poly_collection
polygon
pool
predef
preprocessor
process
program_options
property_map
property_tree
proto
ptr_container
python
qvm
random
range
ratio
rational
ref
regex
result_of
safe_numerics
scope_exit
serialization
signals2
smart_ptr
sort
spirit
stacktrace
statechart
static_assert
string_ref
swap
system
test
thread
throw_exception
timer
tokenizer
tribool
tti
tuple
type_erasure
type_index
type_traits
typeof
units
unordered
utility
uuid
variant
variant2
vmd
wave
winapi
xpressive
yap
)

# set Boost_INCLUDE_DIRS
set(Boost_INCLUDE_DIRS "${CONAN_INCLUDE_DIRS_BOOST}")

if(NOT TARGET Boost::boost)
    add_library(Boost::boost INTERFACE IMPORTED)
    set_property(TARGET Boost::boost PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${Boost_INCLUDE_DIRS})
endif()

if(NOT TARGET Boost::diagnostic_definitions)
    add_library(Boost::diagnostic_definitions INTERFACE IMPORTED)
    if(WIN32)
        set(Boost_LIB_DIAGNOSTIC_DEFINITIONS "-DBOOST_LIB_DIAGNOSTIC")
        set_target_properties(Boost::diagnostic_definitions PROPERTIES INTERFACE_COMPILE_DEFINITIONS "BOOST_LIB_DIAGNOSTIC")
    endif()
endif()

if(NOT TARGET Boost::disable_autolinking)
    add_library(Boost::disable_autolinking INTERFACE IMPORTED)
    if(WIN32)
        set_target_properties(Boost::disable_autolinking PROPERTIES INTERFACE_COMPILE_DEFINITIONS "BOOST_ALL_NO_LIB")
    endif()
endif()

if(NOT TARGET Boost::dynamic_linking)
    add_library(Boost::dynamic_linking INTERFACE IMPORTED)
    if(WIN32)
        set_target_properties(Boost::dynamic_linking PROPERTIES INTERFACE_COMPILE_DEFINITIONS "BOOST_ALL_DYN_LINK")
    endif()
endif()

set(Boost_LIBRARY_DIRS ${CONAN_LIB_DIRS_BOOST})
set(Boost_LIBRARIES "")
set(PKG_INCLUDE_DIRS ${CONAN_INCLUDE_DIRS_BOOST}
$<$<CONFIG:Release>:${CONAN_INCLUDE_DIRS_BOOST_RELEASE}>
$<$<CONFIG:RelWithDebInfo>:${CONAN_INCLUDE_DIRS_BOOST_RELWITHDEBINFO}>
$<$<CONFIG:MinSizeRel>:${CONAN_INCLUDE_DIRS_BOOST_MINSIZEREL}>
$<$<CONFIG:Debug>:${CONAN_INCLUDE_DIRS_BOOST_DEBUG}>)

foreach(component ${Boost_FIND_COMPONENTS})
    unset(_${component}_FOUND CACHE)
    list(FIND Boost_ALL_COMPONENTS ${component} _${component}_FOUND)
    if(_${component}_FOUND GREATER_EQUAL 0)
        string(TOUPPER ${component} component_upper)
        set(boost_target Boost::${component})

        set(Boost_${component_upper}_FOUND TRUE)

        component_to_library(${component})
        if(DEFINED LIB_${component})
            list(APPEND Boost_LIBRARIES "${LIB_${component}}")
            set(Boost_${component_upper}_LIBRARY ${LIB_${component}})
            message(STATUS "Boost_${component_upper}_LIBRARY: ${Boost_${component_upper}_LIBRARY}")
        endif()

        message(STATUS "Boost_${component_upper}_FOUND: ${Boost_${component_upper}_FOUND}")

        if(NOT TARGET ${boost_target})
            add_library("${boost_target}" INTERFACE IMPORTED)

            set_property(TARGET ${boost_target} PROPERTY INTERFACE_INCLUDE_DIRECTORIES
                ${PKG_INCLUDE_DIRS})

            # NOTE: component_upper instead of target_or_group_name_upper is intentional
            set_property(TARGET ${boost_target} PROPERTY INTERFACE_LINK_LIBRARIES
                ${LIB_${component}}
                ${CONAN_SHARED_LINKER_FLAGS_BOOST_LIST}
                ${CONAN_EXE_LINKER_FLAGS_BOOST_LIST}
                $<$<CONFIG:Release>:
                ${LIB_${component}}
                ${CONAN_SHARED_LINKER_FLAGS_BOOST_RELEASE_LIST} ${CONAN_EXE_LINKER_FLAGS_BOOST_RELEASE_LIST}>
                $<$<CONFIG:RelWithDebInfo>:
                ${LIB_${component}}
                ${CONAN_SHARED_LINKER_FLAGS_BOOST_RELWITHDEBINFO_LIST} ${CONAN_EXE_LINKER_FLAGS_BOOST_RELWITHDEBINFO_LIST}>
                $<$<CONFIG:MinSizeRel>:
                ${LIB_${component}}
                ${CONAN_SHARED_LINKER_FLAGS_BOOST_MINSIZEREL_LIST} ${CONAN_EXE_LINKER_FLAGS_BOOST_MINSIZEREL_LIST}>
                $<$<CONFIG:Debug>:${LIB_${component}}
                ${CONAN_SHARED_LINKER_FLAGS_BOOST_DEBUG_LIST}
                ${CONAN_EXE_LINKER_FLAGS_BOOST_DEBUG_LIST}>
                )

            set_property(TARGET ${boost_target} PROPERTY INTERFACE_COMPILE_DEFINITIONS
                ${CONAN_COMPILE_DEFINITIONS_BOOST}
                $<$<CONFIG:Release>:${CONAN_COMPILE_DEFINITIONS_BOOST_RELEASE}>
                $<$<CONFIG:RelWithDebInfo>:${CONAN_COMPILE_DEFINITIONS_BOOST_RELWITHDEBINFO}>
                $<$<CONFIG:MinSizeRel>:${CONAN_COMPILE_DEFINITIONS_BOOST_MINSIZEREL}>
                $<$<CONFIG:Debug>:${CONAN_COMPILE_DEFINITIONS_BOOST_DEBUG}>)

            set_property(TARGET ${boost_target} PROPERTY INTERFACE_COMPILE_OPTIONS
                ${CONAN_C_FLAGS_BOOST_LIST} ${CONAN_CXX_FLAGS_BOOST_LIST}
                $<$<CONFIG:Release>:${CONAN_C_FLAGS_BOOST_RELEASE_LIST} ${CONAN_CXX_FLAGS_BOOST_RELEASE_LIST}>
                $<$<CONFIG:RelWithDebInfo>:${CONAN_C_FLAGS_BOOST_RELWITHDEBINFO_LIST} ${CONAN_CXX_FLAGS_BOOST_RELWITHDEBINFO_LIST}>
                $<$<CONFIG:MinSizeRel>:${CONAN_C_FLAGS_BOOST_MINSIZEREL_LIST} ${CONAN_CXX_FLAGS_BOOST_MINSIZEREL_LIST}>
                $<$<CONFIG:Debug>:${CONAN_C_FLAGS_BOOST_DEBUG_LIST}  ${CONAN_CXX_FLAGS_BOOST_DEBUG_LIST}>)
        endif()
    else()
        message(SEND_ERROR "Boost: ${component} is not a valid component")
    endif()
endforeach()

# extract boost version from boost/version.hpp (boost config)
set(Boost_VERSION 0)
set(Boost_LIB_VERSION "")
if(DEFINED CONAN_INCLUDE_DIRS_BOOST_CONFIG)
  # modular boost packages (bincrafters)
  set(_boost_CONFIG_DIR "${CONAN_INCLUDE_DIRS_BOOST_CONFIG}")
else()
  # monolithic boost package (conan-center)
  set(_boost_CONFIG_DIR "${CONAN_INCLUDE_DIRS_BOOST}")
endif()
file(STRINGS "${_boost_CONFIG_DIR}/boost/version.hpp" _boost_VERSION_HPP_CONTENTS REGEX "#define BOOST_(LIB_)?VERSION ")
unset(_boost_CONFIG_DIR)
set(_Boost_VERSION_REGEX "([0-9]+)")
set(_Boost_LIB_VERSION_REGEX "\"([0-9_]+)\"")
foreach(v VERSION LIB_VERSION)
if("${_boost_VERSION_HPP_CONTENTS}" MATCHES "#define BOOST_${v} ${_Boost_${v}_REGEX}")
  set(Boost_${v} "${CMAKE_MATCH_1}")
endif()
endforeach()
unset(_boost_VERSION_HPP_CONTENTS)

math(EXPR Boost_MAJOR_VERSION "${Boost_VERSION} / 100000")
math(EXPR Boost_MINOR_VERSION "${Boost_VERSION} / 100 % 1000")
math(EXPR Boost_SUBMINOR_VERSION "${Boost_VERSION} % 100")

set(Boost_FOUND TRUE)

message(STATUS "Boost_FOUND: ${Boost_FOUND}")
message(STATUS "Boost_VERSION: ${Boost_VERSION}")
message(STATUS "Boost_LIB_VERSION: ${Boost_LIB_VERSION}")
message(STATUS "Boost_MAJOR_VERSION: ${Boost_MAJOR_VERSION}")
message(STATUS "Boost_MINOR_VERSION: ${Boost_MINOR_VERSION}")
message(STATUS "Boost_SUBMINOR_VERSION: ${Boost_SUBMINOR_VERSION}")
message(STATUS "Boost_INCLUDE_DIRS: ${Boost_INCLUDE_DIRS}")
message(STATUS "Boost_LIBRARY_DIRS: ${Boost_LIBRARY_DIRS}")
message(STATUS "Boost_LIBRARIES: ${Boost_LIBRARIES}")
