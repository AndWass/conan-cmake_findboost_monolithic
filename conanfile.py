from conans import ConanFile


class CMakeFindboostMonolithicConan(ConanFile):
    name = "cmake_findboost_monolithic"
    version = "1.71.0"
    url = "https://gitlab.com/AndWass/conan-cmake_findboost_monolithic"
    description = "Enables use of Boost monolithic package with traditional CMake FindBoost"
    license = "MIT"
    exports = ["LICENSE"]
    exports_sources = ["FindBoost.cmake"]

    def package(self):
        self.copy("FindBoost.cmake")
