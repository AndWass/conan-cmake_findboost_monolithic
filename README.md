## Basic setup


```
$ conan remote add <REMOTE> https://api.bintray.com/conan/andwass/conan 
$ conan install cmake_findboost_monolithic/1.71.0@andwass/stable
```

## Usage in CMake

Use `find_package` in much the same way as if boost was locally installed.

### CMake targets

```
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
find_package(Boost COMPONENTS REQUIRED regex system date_time asio)
target_link_libraries(${PROJECT_NAME} PUBLIC Boost::regex Boost::system Boost::date_time Boost::asio)
```

### CMake variables

```
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
find_package(Boost COMPONENTS REQUIRED regex system date_time asio)
target_include_directories(${PROJECT_NAME} PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC ${Boost_REGEX_LIBRARY} ${Boost_SYSTEM_LIBRARY} ${Boost_DATE_TIME_LIBRARY} ${Boost_ASIO_LIBRARY})
```

## License
[MIT](LICENSE)
